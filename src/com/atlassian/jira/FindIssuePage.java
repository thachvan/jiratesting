package com.atlassian.jira;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class FindIssuePage extends Page {
	public final String PROJECT_PULLED_DOWN_STAGE = "projectPulledDown";
	public final String TYPE_PULLED_DOWN_STAGE = "typePulledDown";
	
	public final String PROJECT_DROPDOWN = "projectDropdown";
	public final String PROJECT_TEXTBOX = "projectTextbox";
	public final String TYPE_DROPDOWN = "typeDropdown";
	public final String TYPE_TEXTBOX = "typeTextbox";
	public final String QUERY_TEXTBOX = "queryTextbox";
	public final String FIND_BUTTON = "findButton";
	
	public FindIssuePage(WebDriver driver) {
		super(driver);
		
		addElement(
				PROJECT_DROPDOWN,
				By.xpath("//*[@id=\"content\"]/div/div[4]/div/form/div[1]/div[1]/div[1]/div[1]/div/div[1]/ul/li[1]/button/div/span"));
		addElement(PROJECT_PULLED_DOWN_STAGE, PROJECT_TEXTBOX,
				By.id("searcher-pid-input"));
		addElement(
				TYPE_DROPDOWN,
				By.xpath("//*[@id=\"content\"]/div/div[4]/div/form/div[1]/div[1]/div[1]/div[1]/div/div[1]/ul/li[2]/button/div/span"));
		addElement(TYPE_PULLED_DOWN_STAGE, TYPE_TEXTBOX,
				By.id("searcher-type-input"));
		addElement(QUERY_TEXTBOX, By.id("searcher-query"));
		addElement(
				FIND_BUTTON,
				By.xpath("//*[@id=\"content\"]/div/div[4]/div/form/div[1]/div[1]/div[1]/div[1]/div/div[1]/ul/li[7]/button/span"));
	}
	
	/**
	 * Select project
	 * 
	 * @param project
	 *            project name
	 * @return true if successful, false otherwise
	 */
	public boolean selectProject(String project) {
		// pull down dropdown list
		getElement(PROJECT_DROPDOWN).click();
		if (!initUI(PROJECT_PULLED_DOWN_STAGE, WAITING_TIME)) {
			return false;
		}
		// input project name
		getElement(PROJECT_PULLED_DOWN_STAGE, PROJECT_TEXTBOX)
				.sendKeys(project);
		getElement(PROJECT_PULLED_DOWN_STAGE, PROJECT_TEXTBOX).sendKeys(
				Keys.RETURN);
		getElement(PROJECT_PULLED_DOWN_STAGE, PROJECT_TEXTBOX).sendKeys(
				Keys.ESCAPE);
		// focus to another control
		getElement(QUERY_TEXTBOX).click();
		// refresh UI
		if (!initUI()) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Select type
	 * 
	 * @param type
	 *            type
	 * @return true if successful, false otherwise
	 */
	public boolean selectType(String type) {
		// pull down dropdown list
		getElement(TYPE_DROPDOWN).click();
		if (!initUI(TYPE_PULLED_DOWN_STAGE, WAITING_TIME)) {
			return false;
		}
		// input project name
		getElement(TYPE_PULLED_DOWN_STAGE, TYPE_TEXTBOX).sendKeys(type);
		getElement(TYPE_PULLED_DOWN_STAGE, TYPE_TEXTBOX).sendKeys(Keys.RETURN);
		getElement(TYPE_PULLED_DOWN_STAGE, TYPE_TEXTBOX).sendKeys(Keys.ESCAPE);
		// focus to another control
		getElement(QUERY_TEXTBOX).click();
		// refresh UI
		if (!initUI()) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Input a text into textbox for searching
	 * 
	 * @param query
	 *            text to input
	 * @return true if successful, false otherwise
	 */
	public boolean inputQuery(String query) {
		getElement(QUERY_TEXTBOX).click();
		getElement(QUERY_TEXTBOX).sendKeys(query);
		// refresh UI
		if (!initUI()) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Click the Search button to start finding issues
	 * 
	 * @return true if successful, false otherwise
	 */
	public boolean startFinding() {
		getElement(FIND_BUTTON).click();
		try {
			Thread.sleep(WAITING_TIME*500);
		} catch (InterruptedException e) {
		}
		// refresh UI
		if (!initUI()) {
			return false;
		}
		
		return true;
	}
}
