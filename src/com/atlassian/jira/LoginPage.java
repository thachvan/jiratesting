package com.atlassian.jira;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends Page {
	private String url = "https://jira.atlassian.com/secure/login.jsp?os_destination=%2Fsecure%2FDashboard.jspa";
	
	public LoginPage(WebDriver driver) {
		super(driver);
		
		addElement("textboxUsername", By.id("username"));
		addElement("textboxPassword", By.id("password"));
		addElement("buttonSubmit", By.id("login-submit"));
	}
	
	/**
	 * Open the page
	 */
	public void open() {
		driver.navigate().to(url);
	}
	
	/**
	 * Perform logging in with an account
	 * 
	 * @param username
	 *            username of account
	 * @param password
	 *            password of account
	 * @return the DashboardPage
	 */
	public DashboardPage login(String username, String password) {
		DashboardPage dashboardPage;
		
		getElement("textboxUsername").sendKeys(username);
		getElement("textboxPassword").sendKeys(password);
		getElement("buttonSubmit").click();
		
		dashboardPage = new DashboardPage(driver, true);
		if (!dashboardPage.initUI(dashboardPage.LOGGED_IN_STAGE, WAITING_TIME)) {
			return null;
		}
		
		return dashboardPage;
	}
}
