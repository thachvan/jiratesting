package com.atlassian.jira;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class JiraTesting {
	private static String username = "thachfx@yahoo.com";
	private static String password = "1qaz@WSX";
	
	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "lib/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		// open dashboard page
		DashboardPage dashboardPage = new DashboardPage(driver, false);
		dashboardPage.open();
		dashboardPage.maximize();
		if (!dashboardPage.initUI(dashboardPage.WAITING_TIME)) {
			System.out.println("Failed to initialize the dashboard page");
			dashboardPage.close();
			
			return;
		}
		
		// login
		LoginPage loginPage = dashboardPage.openLoginPage();
		dashboardPage = loginPage.login(username, password);
		
		// create issue
		
		CreateIssueForm createIssueForm = dashboardPage.openCreateIssueForm();
		
		Issue issue = new Issue();
		issue.project = "A Test Project (TST)";
		issue.issueType = "Bug";
		issue.summary = "Nothing happens when clicking the Create button";
		issue.securityLevel = "Reporter and developers";
		issue.priority = "Critical";
		issue.dueDate = "19/Apr/2015";
		issue.components.add("Component 1");
		issue.components.add("Component 2");
		issue.affectVersions.add("Version 2.0");
		issue.affectVersions.add("Dev Version");
		issue.fixVersions.add("Test Version");
		issue.assignee = "Unassigned";
		issue.environment = "- OS: Windows 7 x64\r\n- Browser: Chrome 42.0.2311.90 m";
		issue.description = "When clicking the Create button, the Create Issue form is not closed. " 
				+ "No new defect is created. This is not correct. "
				+ "New defect should be created successfully.";
		issue.randomText = "any";
		issue.vendorDeliveryDate = "19/May/2015";
		issue.vendor = "Oracle";
		issue.burgerOptions.add("Tomato");
		issue.burgerOptions.add("Onion");
		issue.attachedFiles.add("lib/attached1.txt");
		issue.attachedFiles.add("lib/attached2.txt");
		issue.originalEstimate = "1d 1h";
		issue.animalType = "Fish";
		issue.animalName = "Flathead";
		issue.tester = "";
		issue.mySingleVersionPicker = "Version 2.0";
		issue.labels.add("create");
		issue.labels.add("issue");
		issue.storyPoints = "52";
		issue.sprint = "6.1.6 docs";
		issue.epicLink = "epic with C";
		issue.regularExpression = "123";
		
		String issueHyperlink = createIssueForm.checkCreate(issue);
		//String issueHyperlink = "https://jira.atlassian.com/browse/TST-66091";
		ViewIssuePage viewIssuePage = dashboardPage.openViewIssuePage(issueHyperlink);
		
		// update issue
		
		EditIssueForm editIssueForm = viewIssuePage.openEditIssueForm();
		editIssueForm.updateSummary("New issue is not created when clicking the Create button", false);
		editIssueForm.updateDescription("When clicking the Create button, the Create Issue form is not closed. "
				+ "No new defect is created. This is not correct. New defect should be created successfully. "
				+ "Sometimes this defect doesn't occur.", true);
		viewIssuePage.initUI(viewIssuePage.WAITING_TIME);
		viewIssuePage.checkSummary("New issue is not created when clicking the Create button");
		viewIssuePage.checkDescription("When clicking the Create button, the Create Issue form is not closed. "
				+ "No new defect is created. This is not correct. New defect should be created successfully. "
				+ "Sometimes this defect doesn't occur.");
		
		// find issue
		
		FindIssuePage findIssuePage = dashboardPage.openFindIssuePage();
		findIssuePage.selectProject("A Test Project (TST)");
		findIssuePage.selectType("Bug");
		Thread.sleep(findIssuePage.WAITING_TIME*1000);
		findIssuePage.inputQuery("Create button");
		findIssuePage.startFinding();
		
		System.out.println("Finished");
		dashboardPage.close();
	}
}
