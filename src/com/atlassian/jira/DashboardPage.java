package com.atlassian.jira;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DashboardPage extends Page {
	public final String url = "https://jira.atlassian.com/secure/Dashboard.jspa";
	public final String LOGGED_IN_STAGE = "loggedIn";
	public final String PULL_DOWN_ISSUES_MENU_STAGE = "pullDownIssuesMenu";
	public final String LOGIN_LINK = "loginLink";
	public final String CREATE_BUTTON = "createButton";
	public final String ISSUES_DROPDOWN = "issuesDropdown";
	public final String SEARCH_FOR_ISSUES_MENU_ITEM = "searchForIssuesMenuItem";
	
	public DashboardPage(WebDriver driver, boolean loggedIn) {
		super(driver);
		
		addElement(LOGIN_LINK, By.id("user-options"));
		addElement(LOGGED_IN_STAGE, CREATE_BUTTON,
				By.xpath("//*[@id=\"create_link\"]"));
		addElement(LOGGED_IN_STAGE, ISSUES_DROPDOWN, By.id("find_link"));
		addElement(PULL_DOWN_ISSUES_MENU_STAGE, SEARCH_FOR_ISSUES_MENU_ITEM,
				By.id("issues_new_search_link_lnk"));
	}
	
	/**
	 * Open the page
	 */
	public void open() {
		driver.navigate().to(url);
	}
	
	/**
	 * Open the form "Create Issue"
	 * 
	 * @return the CreateIssueForm
	 */
	public CreateIssueForm openCreateIssueForm() {
		getElement(LOGGED_IN_STAGE, CREATE_BUTTON).click();
		
		CreateIssueForm createIssueForm = new CreateIssueForm(driver);
		if (!createIssueForm.initUI(WAITING_TIME)) {
			return null;
		}
		
		return createIssueForm;
	}
	
	/**
	 * Open the page "Search for Issues"
	 * 
	 * @return the FindIssuePage
	 */
	public FindIssuePage openFindIssuePage() {
		/**
		 * These code lines is using for opening page by clicking on dropdown
		 * menu. Unfortunately, due to an uncertain issue of page, dropdown menu
		 * may not appear. So these code lines have been commented out. Page
		 * will be opened by navigating directly using its URL
		 */
		/*
		 * // pull down menu Issues getElement(LOGGED_IN_STAGE,
		 * ISSUES_DROPDOWN).click(); if (!initUI(PULL_DOWN_ISSUES_MENU_STAGE,
		 * WAITING_TIME)) { return null; } // click "Search for issues"
		 * getElement(PULL_DOWN_ISSUES_MENU_STAGE, SEARCH_FOR_ISSUES_MENU_ITEM)
		 * .click(); try { Thread.sleep(WAITING_TIME * 1000); } catch
		 * (InterruptedException e) { }
		 */
		driver.navigate().to("https://jira.atlassian.com/issues/?jql=");
		
		FindIssuePage findIssuePage = new FindIssuePage(driver);
		if (!findIssuePage.initUI(WAITING_TIME)) {
			return null;
		}
		
		return findIssuePage;
	}
	
	/**
	 * Open the Login page
	 * 
	 * @return the Login Page
	 */
	public LoginPage openLoginPage() {
		LoginPage loginPage = new LoginPage(driver);
		
		getElement(LOGIN_LINK).click();
		if (!loginPage.initUI(WAITING_TIME)) {
			return null;
		}
		
		return loginPage;
	}
	
	/**
	 * Open the issue page
	 * 
	 * @param url
	 *            url to go to issue page
	 * @return the ViewIssuePage
	 */
	public ViewIssuePage openViewIssuePage(String url) {
		ViewIssuePage viewIssuePage = new ViewIssuePage(driver);
		
		driver.navigate().to(url);
		if (!viewIssuePage.initUI(WAITING_TIME)) {
			return null;
		}
		
		return viewIssuePage;
	}
}
