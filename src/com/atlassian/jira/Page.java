package com.atlassian.jira;

import java.util.Date;
import java.util.Set;
import java.util.TreeMap;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Page {
	protected WebDriver driver = null;
	protected TreeMap<String, TreeMap<String, Element>> elements = new TreeMap<String, TreeMap<String, Element>>();
	public final String DEFAULT_STAGE = "default";
	public final int WAITING_TIME = 30; // 30 seconds
	
	public Page(WebDriver driver) {
		this.driver = driver;
	}
	
	/**
	 * Add name of a page element to a TreeMap for managing in default stage
	 * 
	 * @param name
	 *            name of element
	 * @param condition
	 *            condition to recognize element
	 */
	public void addElement(String name, By condition) {
		addElement(DEFAULT_STAGE, name, condition);
	}
	
	/**
	 * Add name of a page element to a TreeMap for managing in specific stage
	 * 
	 * @param stage
	 *            stage contains element
	 * @param name
	 *            name of element
	 * @param condition
	 *            condition to recognize element
	 */
	public void addElement(String stage, String name, By condition) {
		if (elements.get(stage) == null) {
			elements.put(stage, new TreeMap<String, Element>());
		}
		
		Element element = new Element();
		
		element.condition = condition;
		element.webElement = null;
		
		elements.get(stage).put(name, element);
	}
	
	/**
	 * Close the browser window
	 */
	public void close() {
		driver.quit();
	}
	
	/**
	 * Get a WebElement control
	 * 
	 * @param name
	 *            name of element
	 * @return a WebElement control if successful, null otherwise
	 */
	public WebElement getElement(String name) {
		return getElement(DEFAULT_STAGE, name);
	}
	
	public WebElement getElement(String stage, String name) {
		return elements.get(stage).get(name).webElement;
	}
	
	/**
	 * Initialize all required UI elements
	 * 
	 * @return true if successful, false otherwise
	 */
	protected boolean initUI() {
		return initUI(DEFAULT_STAGE);
	}
	
	/**
	 * Initialize all required UI elements in a specific stage
	 * 
	 * @param stage
	 *            stage that UI elements belong to
	 * @return true if successful, false otherwise
	 */
	protected boolean initUI(String stage) {
		if (elements.get(stage) == null) {
			return false;
		}
		
		Set<String> names = elements.get(stage).keySet();
		
		for (String name : names) {
			Element element = elements.get(stage).get(name);
			try {
				element.webElement = driver.findElement(element.condition);
			} catch (NoSuchElementException e) {
				System.out.println("Finding " + name);
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * Initialize all required UI elements within an amount of seconds
	 * 
	 * @param timeoutInSecs
	 *            time out value, in seconds
	 * @return true if successful, false otherwise
	 */
	public boolean initUI(long timeoutInSecs) {
		return initUI(DEFAULT_STAGE, timeoutInSecs);
	}
	
	/**
	 * Initialize all required UI elements in a specific stage within an amount
	 * of seconds
	 * 
	 * @param stage
	 *            stage that UI elements belong to
	 * @param timeoutInSecs
	 *            time out value, in seconds
	 * @return true if successful, false otherwise
	 */
	public boolean initUI(String stage, long timeoutInSecs) {
		boolean run = true;
		Date deadline = new Date(System.currentTimeMillis() + timeoutInSecs
				* 1000);
		
		do {
			// continue execution if not exceed timeout
			if (deadline.after(new Date(System.currentTimeMillis()))) {
				// stop if found all elements
				if (initUI(stage)) {
					return true;
				}
				// otherwise, sleep then continue
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					run = false;
				}
			} else {
				run = false;
			}
		} while (run);
		
		System.out.println("Failed to initialize UI of "
				+ this.getClass().getName());
		
		return false;
	}
	
	/**
	 * Maximize the current browser window and WebDriver
	 */
	public void maximize() {
		driver.manage().window().maximize();
	}
	
	/**
	 * Wait until an element attribute get expected value within an amount of
	 * time
	 * 
	 * @param elementName
	 *            element to check its attribute
	 * @param elementAttribute
	 *            attribute to check
	 * @param expectedValue
	 *            expected value
	 * @param timeoutInSecs
	 *            time out value, in seconds
	 * @return true if element attribute has expected value, false if exceeded
	 *         time out value but still has no expected value
	 */
	public boolean waitForElementAttribute(String elementName,
			String elementAttribute, String expectedValue, long timeoutInSecs) {
		return waitForElementAttribute(DEFAULT_STAGE, elementName,
				elementAttribute, expectedValue, timeoutInSecs);
	}
	
	/**
	 * Wait until an element attribute in a specific stage get expected value
	 * within an amount of time
	 * 
	 * @param stage
	 *            stage of element
	 * @param elementName
	 *            element to check its attribute
	 * @param elementAttribute
	 *            attribute to check
	 * @param expectedValue
	 *            expected value
	 * @param timeoutInSecs
	 *            time out value, in seconds
	 * @return true if element attribute has expected value, false if exceeded
	 *         time out value but still has no expected value
	 */
	public boolean waitForElementAttribute(String stage, String elementName,
			String elementAttribute, String expectedValue, long timeoutInSecs) {
		boolean run = true;
		Date deadline = new Date(System.currentTimeMillis() + timeoutInSecs
				* 1000);
		WebElement element = getElement(stage, elementName);
		String attribute;
		
		do {
			// continue execution if not exceed timeout
			if (deadline.after(new Date(System.currentTimeMillis()))) {
				// stop if element has expected value
				try {
					attribute = element.getAttribute(elementAttribute);
					if (attribute != null && attribute.equals(expectedValue)) {
						return true;
					}
				} catch (StaleElementReferenceException e) {
				}
				// otherwise, sleep then continue
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					run = false;
				}
			} else {
				run = false;
			}
		} while (run);
		
		return false;
	}
	
	/**
	 * Wait until an element appears or exceed timeout
	 * 
	 * @param condition
	 *            By condition to find element
	 * @param timeoutInSecs
	 *            timeout value, in seconds
	 * @return true if found within timeout value, false otherwise
	 */
	public boolean waitForElement(By condition, long timeoutInSecs) {
		boolean run = true;
		Date deadline = new Date(System.currentTimeMillis() + timeoutInSecs
				* 1000);
		WebElement element;
		
		do {
			// continue execution if not exceed timeout
			if (deadline.after(new Date(System.currentTimeMillis()))) {
				// stop if element has expected value
				try {
					element = driver.findElement(condition);
					if (element != null) {
						return true;
					}
				} catch (NoSuchElementException e) {
				}
				// otherwise, sleep then continue
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					run = false;
				}
			} else {
				run = false;
			}
		} while (run);
		
		return false;
	}
	
	/**
	 * Wait until an element appears or exceed timeout
	 * 
	 * @param elementName
	 *            name of element
	 * @param timeoutInSecs
	 *            timeout value, in seconds
	 * @return true if found within timeout value, false otherwise
	 */
	public boolean waitForElement(String elementName, long timeoutInSecs) {
		return waitForElement(DEFAULT_STAGE, elementName, timeoutInSecs);
	}
	
	/**
	 * Wait until an element of a specific stage appears or exceed timeout
	 * 
	 * @param stage
	 *            stage of element
	 * @param elementName
	 *            name of element
	 * @param timeoutInSecs
	 *            timeout value, in seconds
	 * @return true if found within timeout value, false otherwise
	 */
	public boolean waitForElement(String stage, String elementName,
			long timeoutInSecs) {
		boolean run = true;
		Date deadline = new Date(System.currentTimeMillis() + timeoutInSecs
				* 1000);
		Element element = elements.get(stage).get(elementName);
		
		do {
			// continue execution if not exceed timeout
			if (deadline.after(new Date(System.currentTimeMillis()))) {
				// stop if element has expected value
				try {
					if (driver.findElement(element.condition) != null) {
						return true;
					}
				} catch (NoSuchElementException e) {
				}
				// otherwise, sleep then continue
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					run = false;
				}
			} else {
				run = false;
			}
		} while (run);
		
		return false;
	}
}
