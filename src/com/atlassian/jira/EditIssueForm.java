package com.atlassian.jira;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class EditIssueForm extends IssueForm {
	public final String MORE_FIELDS_STAGE = "moreFields";
	public final String YET_MORE_FIELDS_STAGE = "yetMoreFields";
	
	// this control appears in all tab
	public final String COMMENT = "comment";
	public final String UPDATE_BUTTON = "updateButton";
	// default tab - Field tab
	public final String SELECT_FILES = "selectFiles";
	public final String XSP = "xSP";
	// "More fields" tab
	public final String ANIMAL_TYPE = "animalType";
	public final String ANIMAL_NAME = "animalName";
	// "Yet More fields" tab
	public final String BURGER_OPTIONS_LETTUCE = "burgerOptions_Lettuce";
	public final String BURGER_OPTIONS_TOMATO = "burgerOptions_Tomato";
	public final String BURGER_OPTIONS_ONION = "burgerOptions_Onion";
	public final String BURGER_OPTIONS_CHILLI = "burgerOptions_Chilli";
	public final String LARGE_TEXT_BLOCK = "largeTextBlock";
	
	public EditIssueForm(WebDriver driver) {
		super(driver);
		
		addElement(COMMENT, By.id("comment"));
		addElement(UPDATE_BUTTON, By.id("edit-issue-submit"));
		addElement(SELECT_FILES,
				By.xpath("//*[@id=\"tab-0\"]/fieldset/div/div/label"));
		addElement(XSP, By.id("customfield_10421"));
		
		addElement(MORE_FIELDS_STAGE, ANIMAL_TYPE, By.id("customfield_10061"));
		addElement(MORE_FIELDS_STAGE, ANIMAL_NAME, By.id("customfield_10061:1"));
		
		addElement(YET_MORE_FIELDS_STAGE, BURGER_OPTIONS_LETTUCE,
				By.id("customfield_10064-1"));
		addElement(YET_MORE_FIELDS_STAGE, BURGER_OPTIONS_TOMATO,
				By.id("customfield_10064-2"));
		addElement(YET_MORE_FIELDS_STAGE, BURGER_OPTIONS_ONION,
				By.id("customfield_10064-3"));
		addElement(YET_MORE_FIELDS_STAGE, BURGER_OPTIONS_CHILLI,
				By.id("customfield_10064-4"));
		addElement(YET_MORE_FIELDS_STAGE, LARGE_TEXT_BLOCK,
				By.id("customfield_10170"));
	}
	
	/**
	 * Click the Update button if needed
	 * 
	 * @param submit
	 *            click the Update button if true, do nothing if false
	 */
	public void submit(boolean submit) {
		if (submit) {
			getElement(UPDATE_BUTTON).click();
		}
		try {
			Thread.sleep(WAITING_TIME * 200);
		} catch (InterruptedException e) {
		}
	}
	
	/**
	 * Update summary field
	 * 
	 * @param summary
	 *            new summary
	 * @param submit
	 *            click the Update button if true, do nothing if false
	 */
	public void updateSummary(String summary, boolean submit) {
		getElement(SUMMARY).clear();
		getElement(SUMMARY).sendKeys(summary);
		submit(submit);
	}
	
	/**
	 * Update description field
	 * 
	 * @param description
	 *            new description
	 * @param submit
	 *            click the Update button if true, do nothing if false
	 */
	public void updateDescription(String description, boolean submit) {
		getElement(DESCRIPTION).clear();
		getElement(DESCRIPTION).sendKeys(description);
		submit(submit);
	}
}
