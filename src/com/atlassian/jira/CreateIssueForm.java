package com.atlassian.jira;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CreateIssueForm extends IssueForm {
	public final String PROJECT = "project";
	public final String BURGER_OPTIONS_LETTUCE = "burgerOptions_Lettuce";
	public final String BURGER_OPTIONS_TOMATO = "burgerOptions_Tomato";
	public final String BURGER_OPTIONS_ONION = "burgerOptions_Onion";
	public final String BURGER_OPTIONS_CHILLI = "burgerOptions_Chilli";
	public final String SELECT_FILES = "selectFiles";
	public final String ANIMAL_TYPE = "animalType";
	public final String ANIMAL_NAME = "animalName";
	public final String TESTER = "tester";
	public final String MY_SINGLE_VERSION_PICKER = "mySingleVersionPicker";
	public final String LABELS = "labels";
	public final String STORY_POINTS = "storyPoints";
	public final String EPIC_LINK = "epicLink";
	public final String REGULAR_EXPRESSION = "regularExpression";
	public final String CREATE_BUTTON = "createButton";
	
	public CreateIssueForm(WebDriver driver) {
		super(driver);
		
		addElement(PROJECT, By.id("project-field"));
		addElement(BURGER_OPTIONS_LETTUCE, By.id("customfield_10064-1"));
		addElement(BURGER_OPTIONS_TOMATO, By.id("customfield_10064-2"));
		addElement(BURGER_OPTIONS_ONION, By.id("customfield_10064-3"));
		addElement(BURGER_OPTIONS_CHILLI, By.id("customfield_10064-4"));
		addElement(
				SELECT_FILES,
				By.xpath("//*[@id=\"create-issue-dialog\"]/div[2]/div[1]/div/form/div[1]/div[2]/fieldset[2]/div/div/label"));
		addElement(ANIMAL_TYPE, By.id("customfield_10061"));
		addElement(ANIMAL_NAME, By.id("customfield_10061:1"));
		addElement(TESTER, By.id("customfield_10530"));
		addElement(MY_SINGLE_VERSION_PICKER, By.id("customfield_10550"));
		addElement(LABELS, By.id("labels-textarea"));
		addElement(STORY_POINTS, By.id("customfield_10653"));
		addElement(EPIC_LINK, By.xpath("//*[@id=\"customfield_12931-field\"]"));
		addElement(REGULAR_EXPRESSION, By.id("customfield_14130"));
		addElement(CREATE_BUTTON, By.id("create-issue-submit"));
	}
	
	/**
	 * Create a new issue and check if created successfully
	 * 
	 * @param issue
	 *            issue to create
	 * @return URL of newly created issue
	 */
	public String checkCreate(Issue issue) {
		// project
		getElement(PROJECT).sendKeys(issue.project);
		getElement(PROJECT).sendKeys(Keys.RETURN);
		
		waitForElementAttribute(ISSUE_TYPE, "disabled", "false", WAITING_TIME);
		initUI(WAITING_TIME);
		
		// issue type
		getElement(ISSUE_TYPE).sendKeys(issue.issueType);
		getElement(ISSUE_TYPE).sendKeys(Keys.RETURN);
		
		waitForElementAttribute(SUMMARY, "disabled", "false", WAITING_TIME);
		initUI(WAITING_TIME);
		
		// summary
		getElement(SUMMARY).sendKeys(issue.summary);
		
		// security level
		Select securityLevel = new Select(getElement(SECURITY_LEVEL));
		securityLevel.selectByVisibleText(issue.securityLevel);
		
		// priority
		getElement(PRIORITY).sendKeys(issue.priority);
		
		// due date
		getElement(DUE_DATE).sendKeys(issue.dueDate);
		
		// components
		for (String component : issue.components) {
			getElement(COMPONENTS).sendKeys(component);
			getElement(COMPONENTS).sendKeys(Keys.RETURN);
		}
		
		// affects versions
		for (String version : issue.affectVersions) {
			getElement(AFFECTS_VERSION).sendKeys(version);
			getElement(AFFECTS_VERSION).sendKeys(Keys.RETURN);
		}
		
		// fix versions
		for (String version : issue.fixVersions) {
			getElement(FIX_VERSIONS).sendKeys(version);
			getElement(FIX_VERSIONS).sendKeys(Keys.RETURN);
		}
		
		// assignee
		getElement(ASSIGNEE).sendKeys(issue.assignee);
		
		// environment
		getElement(ENVIRONMENT).sendKeys(issue.environment);
		
		// description
		getElement(DESCRIPTION).sendKeys(issue.description);
		
		// random text
		getElement(RANDOM_TEXT).clear();
		getElement(RANDOM_TEXT).sendKeys(issue.randomText);
		
		// vendor delivery date
		getElement(VENDOR_DELIVERY_DATE).sendKeys(issue.vendorDeliveryDate);
		
		// vendor
		Select vendor = new Select(getElement(VENDOR));
		vendor.selectByVisibleText(issue.vendor);
		
		// burger options
		for (int i = 1; i <= 4; i++) {
			WebElement burgerOption = driver.findElement(By
					.id("customfield_10064-" + Integer.toString(i)));
			if (burgerOption.isSelected()) {
				burgerOption.click();
			}
		}
		for (String burgerOption : issue.burgerOptions) {
			getElement("burgerOptions_" + burgerOption).click();
		}
		
		// attach files
		File openFileApp = new File("lib/OpenFile.exe");
		for (String attachedFile : issue.attachedFiles) {
			File file = new File(attachedFile);
			try {
				getElement(SELECT_FILES).click();
				Runtime.getRuntime().exec(
						openFileApp.getAbsolutePath() + " "
								+ file.getAbsolutePath());
				// initUI(WAITING_TIME);
				
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
				}
				
			} catch (IOException e) {
				System.out.println("Error occurs when trying to execute "
						+ openFileApp.getAbsolutePath());
				
				return null;
			}
		}
		
		// original estimate
		getElement(ORIGINAL_ESTIMATE).sendKeys(issue.originalEstimate);
		
		// animal type
		Select animalType = new Select(getElement(ANIMAL_TYPE));
		animalType.selectByVisibleText(issue.animalType);
		
		// animal name
		Select animalName = new Select(getElement(ANIMAL_NAME));
		animalName.selectByVisibleText(issue.animalName);
		
		// tester
		getElement(TESTER).sendKeys(issue.tester);
		
		// my single version picker
		Select mySingleVersionPicker = new Select(
				getElement(MY_SINGLE_VERSION_PICKER));
		mySingleVersionPicker.selectByVisibleText(issue.mySingleVersionPicker);
		
		// labels
		for (String label : issue.labels) {
			getElement(LABELS).sendKeys(label);
			getElement(LABELS).sendKeys(Keys.SPACE);
		}
		
		// story points
		getElement(STORY_POINTS).sendKeys(issue.storyPoints);
		
		// sprint
		getElement(SPRINT).sendKeys(issue.sprint);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
		}
		getElement(SPRINT).sendKeys(Keys.RETURN);
		
		// epic link
		waitForElementAttribute("epicLink", "disabled", "false", WAITING_TIME);
		getElement(EPIC_LINK).sendKeys(issue.epicLink);
		try {
			Thread.sleep(3 * 1000);
		} catch (InterruptedException e) {
		}
		getElement(EPIC_LINK).sendKeys(Keys.DOWN);
		getElement(EPIC_LINK).sendKeys(Keys.RETURN);
		
		// regular expression
		waitForElementAttribute(REGULAR_EXPRESSION, "disabled", "false",
				WAITING_TIME);
		getElement(REGULAR_EXPRESSION).sendKeys(issue.regularExpression);
		
		// click Create button
		getElement(CREATE_BUTTON).click();
		
		// check if successful message appear
		Assert.assertTrue(waitForElement(
				By.xpath("//*[@id=\"aui-flag-container\"]/div/div"),
				WAITING_TIME * 2));
		
		// determine hyperlink of defect and open it
		String successHTML = driver.findElement(
				By.xpath("//*[@id=\"aui-flag-container\"]/div/div"))
				.getAttribute("innerHTML");
		int found = successHTML.indexOf("href=\"");
		String href = successHTML.substring(found + 6,
				successHTML.indexOf("\"", found + 6));
		
		return "https://jira.atlassian.com" + href;
	}
}
