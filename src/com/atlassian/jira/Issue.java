package com.atlassian.jira;

import java.util.ArrayList;
import java.util.List;

public class Issue {
	public String project, issueType, summary, securityLevel, priority, dueDate;
	public List<String> components = new ArrayList<String>();
	public List<String> affectVersions = new ArrayList<String>();
	public List<String> fixVersions = new ArrayList<String>();
	public String assignee, environment, description, randomText, vendorDeliveryDate, vendor;
	public List<String> burgerOptions = new ArrayList<String>();
	public List<String> attachedFiles = new ArrayList<String>();
	public String originalEstimate, animalType, animalName, tester, mySingleVersionPicker;
	public List<String> labels = new ArrayList<String>();
	public String storyPoints, sprint, epicLink, regularExpression;
}
