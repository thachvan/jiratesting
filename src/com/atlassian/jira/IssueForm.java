package com.atlassian.jira;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class IssueForm extends Page {
	public final String AFFECTS_VERSION = "affectsVersions";
	public final String ASSIGNEE = "assignee";
	public final String COMPONENTS = "components";
	public final String DESCRIPTION = "description";
	public final String DUE_DATE = "dueDate";
	public final String ENVIRONMENT = "environment";
	public final String FIX_VERSIONS = "fixVersions";
	public final String ISSUE_TYPE = "issueType";
	public final String ORIGINAL_ESTIMATE = "originalEstimate";
	public final String PRIORITY = "priority";
	public final String RANDOM_TEXT = "randomText";
	public final String SECURITY_LEVEL = "securityLevel";
	public final String SPRINT = "sprint";
	public final String SUMMARY = "summary";
	public final String VENDOR = "vendor";
	public final String VENDOR_DELIVERY_DATE = "vendorDeliveryDate";
	
	public IssueForm(WebDriver driver) {
		super(driver);
		
		addElement(AFFECTS_VERSION,
				By.xpath("//*[@id=\"versions-textarea\"]"));
		addElement(ASSIGNEE, By.id("assignee-field"));
		addElement(COMPONENTS, By.id("components-textarea"));
		addElement(DESCRIPTION, By.id("description"));
		addElement(DUE_DATE, By.id("duedate"));
		addElement(ENVIRONMENT, By.id("environment"));
		addElement(FIX_VERSIONS, By.id("fixVersions-textarea"));
		addElement(ISSUE_TYPE, By.id("issuetype-field"));
		addElement(ORIGINAL_ESTIMATE, By.id("timetracking"));
		addElement(PRIORITY, By.id("priority-field"));
		addElement(RANDOM_TEXT, By.id("customfield_10010"));
		addElement(SECURITY_LEVEL, By.id("security"));
		addElement(SPRINT, By.id("customfield_11930-field"));
		addElement(SUMMARY, By.id("summary"));
		addElement(VENDOR, By.id("customfield_10040"));
		addElement(VENDOR_DELIVERY_DATE, By.id("customfield_10041"));
	}
}
