package com.atlassian.jira;

import org.junit.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ViewIssuePage extends Page {
	public final String EDIT_BUTTON = "editButton";
	
	public final String SUMMARY = "summary";
	public final String DESCRIPTION = "description";
	
	public ViewIssuePage(WebDriver driver) {
		super(driver);
		
		addElement(EDIT_BUTTON, By.id("edit-issue"));
		addElement(SUMMARY, By.id("summary-val"));
		addElement(DESCRIPTION, By.xpath("//*[@id=\"description-val\"]/div/p"));
	}
	
	/**
	 * Open the form "Edit Issue"
	 * 
	 * @return the EditIssueForm
	 */
	public EditIssueForm openEditIssueForm() {
		EditIssueForm editIssueForm = new EditIssueForm(driver);
		
		getElement(EDIT_BUTTON).click();
		if (!editIssueForm.initUI(WAITING_TIME)) {
			return null;
		}
		
		return editIssueForm;
	}
	
	/**
	 * Assert that current summary is same as expected value
	 * 
	 * @param expectedSummary
	 *            expected summary
	 */
	public void checkSummary(String expectedSummary) {
		String currentSummary = getElement(SUMMARY).getText();
		Assert.assertTrue(expectedSummary.equals(currentSummary));
	}
	
	/**
	 * Assert that current description is same as expected value
	 * 
	 * @param expectedDescription
	 *            expected description
	 */
	public void checkDescription(String expectedDescription) {
		String currentDescription = getElement(DESCRIPTION).getText();
		Assert.assertTrue(expectedDescription.equals(currentDescription));
	}
}
