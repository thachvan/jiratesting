# README #

This project aimed to demonstrate how to use Selenium to test some functions of Jira system.

### Environment ###

* IDE: latest version of Eclipse - Luna Service Release 1a (4.4.1)

* Browser: latest version of Chrome - 42.0.2311.90 m

* Java: latest version of Java 8 - 1.8.0_45-b14

### Setup ###

* Executing jar file requires "lib" folder at the same directory

   |--- ..\

      |--- JiraTesting.jar

      |--- lib

            |--- attached1.txt

            |--- attached2.txt

            |--- chromedriver.exe

            |--- OpenFile.exe

            |--- selenium-server-standalone-2.45.0.jar

* The testing doesn't require any other special setups

### Known issues ###
* Login:

Sometimes after logging in, system doesn't recognize that user has been logged in. The Login link still appears and user has permission same as anonymous user. If you face this case, please wait until the Jira server becomes stable then restart the works.

* Issues dropdown:

The dropdown menu "Issues" may not be pulled down when clicking it. So in this project, I have to allow navigating directly to the page "Search for issues".

* Create issue:

When inputting data into the form "Create Issue", some fields causes the form to be busy after changing its value. Automation script has to wait for a quite long time ~30 seconds to continue working on. Please do not stop automation script when it seems to be stuck on the current form. It still works.